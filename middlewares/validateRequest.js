var jwt = require('jwt-simple');

module.exports = function(req, res, next) {

  // When performing a cross domain request, you will recieve
  // a preflighted request first. This is to check if our the app
  // is safe. 

  // We skip the token outh for [OPTIONS] requests.
  //if(req.method == 'OPTIONS') next();

  var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
  if (token) {
    try{
      var decoded = jwt.decode(token, 'jpay-serect-string');
      if (decoded.exp <= Date.now()) {
        res.json( {
          "status": 1000,
          "message": "Token Expired"
        });
        return;
      }else{
        next();
      }
    }catch(err){
      res.json({
      "status": 1002,
      "message": "Opps!! Something went wrong!"
    });
    }  
  } else {
    res.json({
      "status": 1001,
      "message": "Invalid Token"
    });
    return;
  }
};
