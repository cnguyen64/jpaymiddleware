var router = require('express').Router();
var fs = require('fs-extra');
var avarta_paul = "./asset/images/paul.png",
	avarta_mandy = "./asset/images/mandy.png",
	avarte_katherine = "./asset/images/katherine.png",
	MEDIBANK_logo = "./asset/images/MEDIBANK_logo.png",
	TAC_logo = "./asset/images/TAC_logo.png",
	provider_logo = "./asset/images/provider_logo.png";
var Files = {
	
	writeFile : function(req, res){
		res.json({paul : base64_encode(avarta_paul), mandy : base64_encode(avarta_mandy),
			katherine : base64_encode(avarte_katherine), MEDIBANK : base64_encode(MEDIBANK_logo),
			TAC : base64_encode(TAC_logo), provider_logo : base64_encode(provider_logo)
		});
	}

};

function base64_encode(file) {
    // read binary data
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap).toString('base64');
}

router.get('/writeFile', Files.writeFile);
module.exports = exports = router;