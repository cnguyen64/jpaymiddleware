var router = require('express').Router();
//create del all whitespace of string function
String.prototype.delAllWhiteSpace = function() {
    return this.split(' ').join('');
};
var Customers = {
	/*
	 * get customer data stored by IBM Data Service.
	 * /customer/:jpay_no
	 *
	 */
	 getCustomerByJpayCard: function(req, res){
		var query = req.data.Query.ofType('Customer'),
			jpay_no = (req.params.jpay_no).delAllWhiteSpace();
		query.find({jpay_no: jpay_no}, {limit: 1}).done(function(customer) {
			if (customer.length==1) {
				res.json({success: true, customer: customer[0].attributes});
			}else {
				res.json({ success: false, message:'Dont have any customer with jpay_no: ' + jpay_no});
			}
		}, function(err){
		    res.json(err);
		});
	},
	 getCustomerByOtherField: function(req, res){
		var query     = req.data.Query.ofType('Customer'),
			firstname = (req.params.firstname).delAllWhiteSpace(),
			lastname  = (req.params.lastname).delAllWhiteSpace(),
			dob       = (req.params.dob).delAllWhiteSpace(),
			postcode  = (req.params.postcode).delAllWhiteSpace();
		query.find({firstname: firstname, lastname: lastname, dob: dob, postcode: postcode }, {limit: 1}).done(function(customer) {
			if (customer.length==1) {
				res.json({success: true, customer: customer[0].attributes});
			}else {
				res.json({ success: false, message:'Dont have any customer with firstname: ' + firstname +'lastname :' + lastname +'dob :'+ dob + 'postcode :' + postcode});
			}
		}, function(err){
		    res.json(err);
		});
	},
	 updateHistory: function(req, res){
	 	//TODO
	 	//update history for each visit of customer

	 },
	 createCustomer: function(req, res){
	 	var id = req.body.id,
	 		jpay_no = req.body.jpay_no,
	 		device_id =req.body.device_id,
	 		firstname = req.body.first_name,
	 		lastname = req.body.last_name,
	 		gender = req.body.gender;
	 	var queryCustomer = req.data.Query.ofType('Customer');
	 	queryCustomer.find({id : id}).then(function(customerReturn){
	 		if(customerReturn.length > 0){
	 			res.json({message :'This customer already exist.' ,customer : customerReturn[0].attributes});
	 		}else{
	 			var payerQuery = req.data.Query.ofType('Payer');
	 			payerQuery.find().then(function(payerReturn){
	 				if(payerReturn.length > 0){
	 					var payers = []
	 					payerReturn.forEach(function(payerObject){
	 						payers.push(payerObject.attributes);
	 					});
	 					var cuctomerObject = {
	 						id:id,
	 						jpay_no: jpay_no,
					        firstname: firstname,
					        lastname:lastname,
					        name: firstname + ' ' + lastname,
					        gender: gender,
					        dob: "1980-01-01",
					        postcode: "70000",
					        tac_no: "T258",
					        device_id: device_id,
					        address: {
					            address_1: "34 High Street Rd",
					            address_2: "Glen Waverley VIC 3150",
					            address_3: ""
				        	},
				        	payer: payers
	 					};
	 					var customerStore = req.data.Object.ofType('Customer',cuctomerObject);
	 					customerStore.save().done(function(customerSaved){
	 						res.json({success : true, message:'Create customer success!', customer:customerSaved.attributes});	
	 					}, function(err){
	 						res.json(err);
	 					});
	 				}else{
	 					var cuctomerObject = {
	 						id:id,
	 						jpay_no: jpay_no,
					        firstname: "Bev",
					        lastname:"Bond",
					        name: "Bev Bond",
					        gender: "Female",
					        dob: "1980-01-01",
					        postcode: "70000",
					        tac_no: "T123",
					        device_id: device_id,
					        address: {
					            address_1: "34 High Street Rd",
					            address_2: "Glen Waverley VIC 3150",
					            address_3: ""
				        	},
				        	payer: ''
	 					};
	 					var customerStore = req.data.Object.ofType('Customer',cuctomerObject);
	 					customerStore.save().done(function(customerSaved){
	 						res.json({success : true, message:'Create customer success!', customer:customerSaved.attributes});	
	 					}, function(err){
	 						res.json(err);
	 					});
	 				}
	 			}, function(err){
	 				res.json(err);
	 			});
	 		}
	 	}, function(err){
	 		res.json(err);
	 	});
	 },
	 updateCustomer : function(req, res){
	 	var mobile = req.body.mobile,
	 		email = req.body.email,
	 		checkin_by = req.body.checkin_by,
	 		id = req.body.id;
	 	var queryCustomer = req.data.Query.ofType('Customer');
	 	queryCustomer.find({id:id}).then(function(returnCustomer){
	 		if(returnCustomer.length > 0){
	 			returnCustomer[0].set('mobile', mobile);
	 			returnCustomer[0].set('email', email);
	 			returnCustomer[0].set('checkin_by', checkin_by);
	 			returnCustomer[0].save().done(function(customerSaved){
	 				res.json({success : true, message: 'Update customer info successful', customer : customerSaved.attributes});
	 			}, function(err){
	 				res.json(err);
	 			});
	 		}else{
	 			res.json({success: false, message:'Dont have customer with id: '+ id});
	 		}
	 	}, function(err){
	 		res.json(err);
	 	});	
	 }
};

router.get('/api/v1/customer/:jpay_no',    	Customers.getCustomerByJpayCard);

router.get('/api/v1/customer/:firstname/:lastname/:dob/:postcode',    	Customers.getCustomerByOtherField);

router.post('/customer',    	Customers.createCustomer);

router.put('/customer',    	Customers.updateCustomer);


module.exports = exports = router;