var router = require('express').Router();

var Push = {
	PushNotificationToConsumer: function(req, res) {
		var payment_id = req.body.payment_id.toString(),
			jpay_no = req.body.jpay_no;
		var message = {
		    alert : "You have a request for payment.",
		    url : payment_id
		};

		var queryCustomer = req.data.Query.ofType('Customer');
		queryCustomer.find({jpay_no : jpay_no}).done(function(customerReturn){
			if(customerReturn.length > 0){
				var device_id = customerReturn[0].attributes.device_id.toString();
				req.ibmpush.sendNotificationByDeviceIds(message, [device_id], null).then(function(response){
		    		res.json({success : true, message : 'Send notification to patient with device_id:' +device_id+ ' successful ' });
				},function(err){                      
					res.json(err);
				});
			}else{
				res.json('Opps!Dont have customer with jpay_no: '+ jpay_no);
			}
		}, function(err){
			res.json(err);
		})
		// Send the notification to devices based on platforms. Allowed values are "A" for iOS device and "G" for Android devices
		
		
	}
};

router.post('/api/v1/pushnotication/consumer/authorize',    	Push.PushNotificationToConsumer);


module.exports = exports = router;