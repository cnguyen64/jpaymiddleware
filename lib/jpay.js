var router = require('express').Router(),
	qr = require('qr-image'),
	fs = require('fs-extra'),
	low    = 1000000000000000,
	high   = 9999999999999999;

//create random number JPAY.
	function randomIntInc (low, high) {
    	return Math.floor(Math.random() * (high - low + 1) + low);
	}
var JPay = {
	GenerateJPayNumber : function(req, res){
		var jpay_no = randomIntInc(low, high);
		var qr_string = qr.imageSync(jpay_no.toString());
		
		var decodedImage = new Buffer(qr_string, 'base64');
    	fs.writeFile('./asset/'+jpay_no.toString()+'.jpg', decodedImage, function(err) {
    		if(!err){

    			var qr_url = './asset/'+jpay_no.toString()+'.jpg';
                var qrQuery = req.data.Query.ofType('Qrcode');
                qrQuery.find({jpay_no : jpay_no}).then(function(qrReturn){
                    if(qrReturn.length > 0){
                        res.json({success: true, cardInfo: qrReturn[0].attributes});
                    }else{
                        var qrStore = req.data.Object.ofType('Qrcode',{
                            qr_code : base64_encode(qr_url),
                            jpay_no: jpay_no,
                            valid_from: '12/15',
                            expire: '12/20'
                        });
                        qrStore.save().done(function(qrSaved){
                            console.log('save qrcode');
                            res.json({success : true, cardInfo : qrSaved.attributes});
                        }, function(err){
                            res.json(err)
                        });
                    }
                }, function(er){
                    res.json(err);
                });	
    		}else{
    			res.json(err);
    		}
    	});
    		
	}

};

function base64_encode(file) {
    // read binary data
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap).toString('base64');
}
router.get('/jpay/jpaynumber',    	JPay.GenerateJPayNumber);

module.exports = exports = router;	