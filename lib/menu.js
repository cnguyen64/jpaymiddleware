var router = require('express').Router();
var Menu = {
	getMenu: function(req, res){
		var type = req.params.type;
		var query = req.data.Query.ofType('Menu');
		query.find({type : type}).done(function(menuReturn){
			if(menuReturn.length > 0){
				res.json({success: true, menu: menuReturn[0].attributes.menu});
			}else{
				res.json({success: false, message: 'dont have menu with type :' + type });
			}	
		}, function(err){	
			res.json(err);
		});
	}
};

router.get('/menu/:type',    	Menu.getMenu);


module.exports = exports = router;