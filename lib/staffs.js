var router = require('express').Router();
var jwt = require('jwt-simple');

var Staffs = {
	/*
	 * List all staffs data of provider stored by IBM Data Service.
	 * /provider/:provider_id/staffs
	 * 
	 */
	 getStaffs: function(req, res){
		var query = req.data.Query.ofType('Staff');
		query.find({provider_id: req.params.provider_id}).done(function(staffs) {
			if (staffs.length>0) {
				var staff_object = [];
				staffs.forEach(function(staffObject){
					staff_object.push(staffObject.attributes);
				});
				res.json({success: true, staffs: staff_object});
			}
			else {
				
				res.json({ success: false, message:'Dont have any staffs'});
			}
		}, function(err){
		    res.json(err);
		});
	},
	/*
	 * Information of staff of provider stored by IBM Data Service.
	 * /provider/:provider_id/staff/:id
	 * 
	 */
	 getStaffById: function(req, res){
		var query = req.data.Query.ofType('Staff');
		query.find({provider_id: req.params.provider_id, _id:req.params.id }).done(function(staffs) {
			if (staffs.length>0) {
				res.json({success: true, staff: staffs[0].attributes});
			}
			else {
				res.json({ success: false, message:'Dont have staff with id: '+eq.params.id});
			}
		}, function(err){
		    res.json(err);
		});
	},
	/*
	 * Update time login/logout of staff
	 * /provider/:provider_id/staffs/:staff_id/:type
	 * 
	 */
	 updateTimeStaff: function(req, res){	
		var query = req.data.Query.ofType('Staff');
		var	date = new Date();
		query.find({_id: req.params.staff_id, provider_id: req.params.provider_id}, {limit :1 }).then(function(staff) {
			if(staff.length > 0){
				staff.forEach(function(person){
					if(req.params.type =='login'){
						person.set('lastlogin', date);	
					}else{
						person.set('lastlogout', date);	
					}
					person.save().done(function(updated) {
						res.json({success: true , staff : updated.attributes});
					}, function(err){
					    res.json(err);
					});
				});
			}else{
				res.json({ success: false, message:'Dont have staff with id: '+ req.params.staff_id + ' at provider: '+ req.params.provider_id});
			}
		}, function(err){
		    res.json(err);
		});
	},
	/*
	 * Login API
	 * /staff/login
	 * @param : staff_id
	 * @params: provider_id 
	 * @param : password
	 * 
	 */
	 login : function(req, res){
	 	var query = req.data.Query.ofType('Staff');
	 	var password = req.body.password;
	 	if(password == '123'){
	 		query.find({_id: req.body.staff_id, provider_id: req.body.provider_id}, {limit :1 }).done(function(staff) {
				if(staff.length == 1){
					res.json({success: true, message: 'Login success', staff : genToken(staff[0].attributes)});
					// res.json({success: true, message: 'Login success', staff : staff[0].attributes});
				}else{
					res.json({ success: false, message:'Login false' });
				}
			}, function(err){
			    res.send(err);
			});
	 	}else{
	 		res.json({ success: false, message:'Login false' });
	 	}
	 	
	 }
};
// private method
function genToken(staff) {
  var expires = expiresIn(1); // 7 days
  var token = jwt.encode({
    exp: expires
  }, 'jpay-serect-string');

  return {
    token: token,
    expires: expires,
    staff: staff
  };
}

function expiresIn(numDays) {
  var dateObj = new Date();
  return dateObj.setDate(dateObj.getDate() + numDays);
}
router.get('/provider/:provider_id/staffs',    				Staffs.getStaffs);

router.get('/provider/:provider_id/staff/:id',    				Staffs.getStaffById);

router.put('/provider/:provider_id/staffs/:staff_id/:type',	Staffs.updateTimeStaff);

router.post('/staff/login', 								Staffs.login);

module.exports = exports = router;


	

