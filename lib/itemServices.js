var router = require('express').Router();

var Items = {
	/*
	 * get options data of provider stored by IBM Data Service.
	 * /options/:provider_id
	 * 
	 */
	 getItems: function(req, res){
		var query = req.data.Query.ofType('Item');
		query.find({provider_type: req.params.provider_type}).done(function(items) {
			if (items.length >0) {
				var item_object = [];
				items.forEach(function(itemObject){
					item_object.push(itemObject.attributes);
				});
				res.json({success: true, items: item_object});
			}else {
				res.json({ success: false, message:'Dont have any items with provider_type: ' + req.params.provider_type + 'and provider_id: ' + req.params.provider_id});
			}
		}, function(err){
		    res.send(err);
		});
	}

};

router.get('/api/v1/providertype/:provider_type/items',    	Items.getItems);

module.exports = exports = router;