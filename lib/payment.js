var router = require('express').Router();

var Payment = {
	getPaymentById: function(req, res){
		var payment_id = parseInt(req.params.payment_id);
		var queryPayment = req.data.Query.ofType('Payment');
		queryPayment.find({payment_id: payment_id}).done(function(payment){
			if(payment.length >0){
				res.json({success: true, payment: payment[0].attributes});
			}else{
				res.json({success: false, message: 'Dont have payment with id :' + payment_id});
			}
		}, function(err){
			res.json(err);
		});
	},
	getPaymentByJpayNoAndStatus: function(req, res){
		var jpay_no = parseInt(req.params.jpay_no);
		var queryPayment = req.data.Query.ofType('Payment');
		queryPayment.find({jpay_no: jpay_no, status :'pending'}).done(function(payment){
			// console.log(payment);
			if(payment.length >0){
				res.json({success: true, payment: payment[0].attributes});
			}else{
				res.json({success: false, message: 'Dont have pending payment with jpay_no :' + jpay_no});
			}
		}, function(err){
			res.json(err);
		});
	},
	getListPayment:function(req,res){
		var queryPayment = req.data.Query.ofType('Payment');
		queryPayment.find().done(function(payment){
			if(payment.length >0){
				res.json({success: true, payment: payment});
			}else{
				res.json({success: false, message: 'Dont have any payment so far'});
			}
		}, function(err){
			res.json(err);
		});
	},
	updatePaymentStatus: function(req, res){
		var payment_id = parseInt(req.params.payment_id);
		var queryPayment = req.data.Query.ofType('Payment');
		queryPayment.find({payment_id:payment_id}).done(function(payment){
			if(payment.length >0){
				payment[0].set('status', 'approved');
				payment[0].save().done(function(paymentSaved){
					res.json({success: true, message:'Payment id:' + payment_id + ' was approved', payment: paymentSaved.attributes});
				}, function(err){
					res.json(err)
				});
			}else{
				res.json({success: false, message: 'Dont have payment with id: ' + payment_id});
			}
		}, function(err){
			res.json(err);
		});
	}
};

router.get('/payment/:payment_id',    	Payment.getPaymentById);
router.get('/payment',    	Payment.getListPayment);
router.put('/payment/:payment_id' ,  Payment.updatePaymentStatus);
router.get('/payment/:jpay_no/pending' ,  Payment.getPaymentByJpayNoAndStatus);

module.exports = exports = router;