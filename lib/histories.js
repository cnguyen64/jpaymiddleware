var router = require('express').Router();
//create del all whitespace of string function
String.prototype.delAllWhiteSpace = function() {
    return this.split(' ').join('');
};
var Histories = {
	getHistory: function(req, res){
		var query     = req.data.Query.ofType('History');
		var	jpay_no    = (req.params.jpay_no).delAllWhiteSpace();
		//start query	
		query.find({jpay_no : jpay_no}).done(function(histories){
			if(histories.length > 0){
				histories.sort(function(a, b){
 					var dateA=new Date(a.attributes.date),
 						dateB=new Date(b.attributes.date);
 					return dateB-dateA; //sort by date ascending
				});
				var history = [];
				histories.forEach(function(historyObject){
					history.push(historyObject.attributes);
				});
				res.json({success: true, history: history})
			}else{
				res.json({success: false, message:'Dont have any history of customer with Jpay number: ' + jpay_no});
			}
		}, function(err){
			res.json(err);
		});
		//end query
	}
};
router.get('/api/v1/history/:jpay_no',    	Histories.getHistory);

module.exports = exports = router;