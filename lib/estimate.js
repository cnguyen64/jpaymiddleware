var router = require('express').Router();

var Estimate = {
	estimateService: function(req, res){
		var item_object = req.body.itemObject,
			id = parseInt(req.body.id);
			// console.log(id);
		var object = JSON.parse(item_object);
		var rateMediBank = 0;
		if(object.length > 0){
			object.forEach(function(itemObject){
				rateMediBank += (parseInt(itemObject.rate))*0.7;

			});
			// console.log(rateMediBank);
		}
		// res.json(object);
		var queryCheckIn = req.data.Query.ofType('CustomerCheckedIn');
		queryCheckIn.find({id: id}).then(function(customer) {
			// console.log(customer);
			if(customer.length  > 0) {
				customer[0].set('item_service_estimate' , object);
				customer[0].save().then(function(customerCheckInSaved) {
					var atb = customerCheckInSaved.attributes;
					var paymentQuery = req.data.Query.ofType('Payment');
					paymentQuery.find({payment_id : id}, {limit: 1}).then(function(paymentReturn){
						// console.log(paymentReturn);
						var payerStore = [];
						if(paymentReturn.length > 0){
							console.log('paymentReturn');
							var payer = paymentReturn[0].attributes.payer;
							// console.log(payer);
							if(payer.length > 0){
								payer.forEach(function(payerObject){
									if(payerObject.payer_id == 1){
			 							var storeObject = { 
				 							payer_id : payerObject.payer_id,
				 							payer_name : payerObject.payer_name,
				 							logo: payerObject.logo,
				 							status:"denied",
				 							message:"Medical excess not reached",
				 							rate: 0,
			 							};
			 							payerStore.push(storeObject);
			 						}else if(payerObject.payer_id == 2){
			 							var storeObject = { 
				 							payer_id : payerObject.payer_id,
				 							payer_name : payerObject.payer_name,
				 							logo: payerObject.logo,
				 							status:"approved",
				 							rate: rateMediBank
			 							};
			 							payerStore.push(storeObject);
			 						}else{
			 							//TODO something
			 						}
								});
							}else{
								//TODO somthing
							}

							paymentReturn[0].set('item_service', atb.item_service_estimate);
							paymentReturn[0].set('payer', payerStore);
							paymentReturn[0].save().done(function(paymentExistSaved){
								res.json({success: true, message: 'estimate service success', payment: paymentExistSaved.attributes});
							}, function(err){
								res.json(err);
							})
						}else{
							console.log('dont have paymentReturn');
							var customerQuery =  req.data.Query.ofType('Customer');
							customerQuery.find({jpay_no : atb.jpay_no}).then(function(customerReturn){
								// console.log(customerReturn);
								if(customerReturn.length > 0 ){
									// console.log(customerReturn[0].attributes.payer);
									customerReturn[0].attributes.payer.forEach(function(payerObject){
										if(payerObject.payer_id == 1){
											console.log('create payer1')
				 							var storeObject = { 
					 							payer_id : payerObject.payer_id,
					 							payer_name : payerObject.payer_name,
					 							logo: payerObject.logo,
					 							status:"denied",
					 							message:"Medical excess not reached",
					 							rate: 0
				 							};
				 							payerStore.push(storeObject);
				 						}else if(payerObject.payer_id == 2){
				 							console.log('create payer2')
				 							var storeObject = { 
					 							payer_id : payerObject.payer_id,
					 							payer_name : payerObject.payer_name,
					 							logo: payerObject.logo,
					 							status:"approved",
					 							rate: rateMediBank
				 							};
				 							payerStore.push(storeObject);
				 						}else{
				 							//TODO something
				 						}
									});
									console.log(atb.jpay_no);
									var payment = req.data.Object.ofType('Payment',{
										jpay_no : parseInt(atb.jpay_no),
										jclaim_reference : 'JClaim'+id,
										claim_receipt_number: 1000 +id,
										payment_id: id,
										status: 'pending',
										item_service: atb.item_service_estimate,
										payer: payerStore
									});
									payment.save().done(function(paymentSaved){
										res.json({success: true, message: 'estimate service success', payment: paymentSaved.attributes});
									}, function(err){
										res.json(err);
									});	
								}
							});						
						}
					}, function(err){

					});
				}, function(err){
					res.json(err)
				});	
			}else{
				res.json({success: false , message : 'Opp!!! something went wrong!'});	
			}	
		}, function(err){
		    res.json(err);
		});	
	}
};

router.post('/api/v1/estimate/estimateservice',    	Estimate.estimateService);

module.exports = exports = router;