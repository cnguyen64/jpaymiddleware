var router = require('express').Router();
//create del all whitespace of string function
String.prototype.delAllWhiteSpace = function() {
    return this.split(' ').join('');
};

var Providers = {
	/*
	 * get provider data stored by IBM Data Service.
	 * /provider/:provider_id
	 * 
	 */
	 getProvider: function(req, res){

		var query = req.data.Query.ofType('Provider');
		query.find({_id: req.params.provider_id}, {limit: 1}).done(function(provider) {
			if (provider.length==1) {
				res.send({success: true, provider: provider[0].attributes});
			}else {
				res.json({ success: false, message:'Dont have any provider with id: ' + req.params.provider_id});
			}
		}, function(err){
		    res.send(err);
		});
	},
	 /*
	 * get list customer was checked-in.
	 * /customer/checkedin
	 * 
	 */
	 checkedInList: function(req, res){
	 	var query = req.data.Query.ofType('CustomerCheckedIn');
	 	console.log(req.params.provider_id);
	 	query.find({provider_id : req.params.provider_id, status : 'checkedin'}).done(function(customers) {
			if (customers.length > 0) {
				var customer_object = [];
				customers.forEach(function(customerObject){
					customer_object.push(customerObject.attributes);
				});
				res.json({success: true, customer: customer_object});
			}else {
				res.json({success: false, message:'Dont have any customer checked-in so far'});
			}
		}, function(err){
		    res.json(err);
		});
	 },
	 checkIn: function(req, res){
	 	var jpay_no = (req.body.jpay_no).delAllWhiteSpace(),
	 		reason = req.body.reason,
	 		tac_no = req.body.tac_no,
	 		provider_id = req.body.provider_id,
	 		name = req.body.name,
	 		checked_in_by = req.body.checked_in_by;
	 		// health_care = req.body.health_care;
	 	var historyObject = { 
				jpay_no: jpay_no,	 			
				date: new Date(),
				reason: reason,
				tac_no: tac_no
			};
		var payer = [];

	 	var customerHistory = req.data.Object.ofType('History', historyObject);
	 	customerHistory.save().then(function(historyObjectSaved){
	 		var customerQuery = req.data.Query.ofType('Customer');
	 		customerQuery.find({jpay_no: jpay_no}, {limit : 1}).then(function(customerQueryReturn){
	 			if(customerQueryReturn.length == 1){
	 				payer = customerQueryReturn[0].attributes.payer;
	 				var payer_store = [];
	 				if(payer.length > 0){
	 					payer.forEach(function(payerObject){
	 						var storeObject = { 
		 						payer_id : payerObject.payer_id,
		 						payer_name : payerObject.payer_name,
		 						logo: payerObject.logo
	 						}
	 						payer_store.push(storeObject);
	 					});
	 				}
					var queryCheckIn = req.data.Query.ofType('CustomerCheckedIn');
					queryCheckIn.find({provider_id : provider_id}).then(function(customers) {
						if (customers.length > 0) {
							var customer = req.data.Object.ofType('CustomerCheckedIn', {
								id: customers.length + 1,
								provider_id: provider_id,
								jpay_no: jpay_no,
								name: name,
								checked_in_time: new Date(),
		       					checked_out_time:"",
								checked_in_by: checked_in_by,
								status: 'checkedin',
								time_interval: '',
								item_service_estimate: '',
								payer:payer_store
							});
							customer.save().done(function(customerCheckInSaved) {
								res.json({success: true, message: 'checked-in your customer success', customer: customerCheckInSaved.attributes});
							}, function(err){
								res.json(err)
							});	
						}else{
							var customer = req.data.Object.ofType('CustomerCheckedIn', {
								id: customers.length + 1,
								provider_id: provider_id,
								jpay_no: jpay_no,
								name: name,
								checked_in_time: new Date(),
		       					checked_out_time:"",
								checked_in_by: checked_in_by,
								status: 'checkedin',
								time_interval: '',
								item_service_estimate: '',
								payer:payer_store
							});
							customer.save().done(function(customerCheckInSaved) {
								res.json({success: true, message: 'checked-in your customer success', customer: customerCheckInSaved.attributes});
							}, function(err){
								res.json(err)
							});	
						}	
					}, function(err){
					    res.json(err);
					});		 				
	 			}else{
	 				res.json({ success: false, message:'Dont have any customer with jpay_no: ' + jpay_no});
	 			}
	 		},function(err){
	 			res.json(err);
	 		});
		}, function(err){
			res.json(err);
		});	
	},
	checkOut : function(req, res){
		var id = parseInt(req.params.id);
		var queryCheckIn = req.data.Query.ofType('CustomerCheckedIn');
			queryCheckIn.find({id : id}).then(function(customers) { 
				if(customers.length > 0){
					customers[0].set('status', 'checkedout');
					customers[0].save().done(function(customerSaved){
						res.json({success : true, message: 'Check out customer successful!'});
					}, function(err){
						res.json(err);
					});
				}else{
					res.json({success: false, message: 'Dont have checkedin customer with id: '+ id});
				}
			},function(err){
				res.json(err);
			});
	}

};

router.get('/provider/:provider_id',    	Providers.getProvider);

router.get('/api/v1/provider/:provider_id/customercheckedin',    	Providers.checkedInList);

router.post('/api/v1/provider/staff/checkincustomer',    	Providers.checkIn);

router.put('/api/v1/provider/staff/checkoutcustomer/:id',    	Providers.checkOut);

module.exports = exports = router;