var router = require('express').Router();
var Claim = {
	getCustomerByID: function(req, res){
		var id = req.params.id;
		var queryCustomer = req.data.Query.ofType('CustomerCheckedIn');
		queryCustomer.find({id : id}, {limit: 1}).done(function(customerReturn){
			if(customerReturn.length == 1){
				res.json({success: true, customer: customerReturn[0].attributes});
			}else{
				res.json({success: false, message: 'dont have customer with id :' + id });
			}	
		}, function(err){	
			res.json(err);
		});
	}
};

router.get('/api/v1/claim/customer/:id',    	Claim.getCustomerByID);


module.exports = exports = router;